/**
 * Licensed Materials - Property of IBM
 *
 * IBM Cognos Products: BI Glass
 *
 * Copyright IBM Corp. 2015
 *
 * US Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */
define(['q',
	'text!./SampleWelcomeView.html',
	], function(Q, html) {
	'use strict';

	var ContentView = function(){
		
		/**
		 * Called by the AppController whenever this view is created
		 *
		 * @public
		 * @returns {Promise} promise resolved to the root DOM element for this view.
		 */
		this.open = function(context, options) {
			this.logger = context.logger;
			this.options = options;
			var deferred = Q.defer();
				
			var root = document.createElement('div');
			root.setAttribute('class','welcome');
			
			root.innerHTML = html;
			deferred.resolve(root);
			return deferred.promise;
		};
		
		/**
		 * Called by the AppController whenever this view is destroyed
		 *
		 * @public
		 */
		this.close = function() {
			this.logger.info('close');
		};
		
		/**
		 * Called by the AppController whenever this view is shown
		 *
		 * @public
		 */
		this.onShow = function() {
			this.logger.info('onShow');
		};
		
		/**
		 * Called by the AppController whenever this view is hidden
		 *
		 * @public
		 */
		this.onHide = function() {
			this.logger.info('onHide');
		};
		
		/**
		 * Called by the AppController whenever display Info is required for this view
		 *
		 * @public
		 * @returns {Object} displayInfo - The displayInfo for this view.
		 * @returns {string} displayInfo.title - The title.
		 * @returns {string} displayInfo.icon - The icon.
		 */
		this.getDisplayInfo = function() {
			this.logger.info('getDisplayInfo');
			return {
				'title':this.options.info.title,
				'icon': this.options.info.icon
			};
		};

	};

	return ContentView;

});
