If any of your sample uploaded files become corrupted, you can download and replace the original file from our Community Blog:
https://community.ibm.com/community/user/businessanalytics/participate/blogs/blogviewer?BlogKey=FF811D76-ABE0-4DF2-BCEA-917176FD72E4


For more information on replacing a file, see https://www.ibm.com/support/knowledgecenter/en/SSEP7J_11.1.0/com.ibm.swg.ba.cognos.ca_gtstd.doc/t_gtsd_upload_files_updates.html



